package org.mozilla.vrbrowser;

import android.app.KeyguardManager;
import android.app.admin.DevicePolicyManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class launchAtStartup extends BroadcastReceiver {
    public DevicePolicyManager mDevicePolicyManager;
    @Override
    public void onReceive(Context context, Intent intent) {
        // Only relaunch the app at device startUp if lockTaskMode
        mDevicePolicyManager = (DevicePolicyManager)
                context.getSystemService(Context.DEVICE_POLICY_SERVICE);
        KeyguardManager kgMgr =
                (KeyguardManager) context.getSystemService(Context.KEYGUARD_SERVICE);
        boolean isDeviceLocked = kgMgr.inKeyguardRestrictedInputMode();
        if(mDevicePolicyManager.isLockTaskPermitted(context.getPackageName())&& !isDeviceLocked) {
            if (intent.getAction().equals(Intent.ACTION_BOOT_COMPLETED)) {
                Intent i = new Intent(context, VRBrowserActivity.class);
                i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                context.startActivity(i);
            }
        }
    }

}